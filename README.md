# Git-Crypt Demo

Just testing the tool: Git-Crypt

Both **file1-plaintext.txt** and **file1-encrypted.txt** have the same content: 
```
Hello world 1
```

## Cloning someone's project
Easy way to decrypt the files after cloning the project:
1. Put the folder **git-crypt** back to the .git folder 
   1. Ask the owner (or someone who has it) to send it to you via [Keybase](https://keybase.io/) 
2. Run the command: `git-crypt unlock .git/git-crypt/keys/default`
    1. You must have the git-crypt tools. Run the command in **1. Install tools** 


## Steps to recreate this project 

1. Install tools
    ```bash
    sudo apt install git-crypt gnupg
    ```

2. Initiate Git Crypt repo
    ```bash
    git-crypt init
    ```

3. Create the .gitattributes and add the files there

4. Check encription status. The files are not yet encrypted: `*** WARNING: staged/committed version is NOT ENCRYPTED! ***` 
    ```bash
    git-crypt status -e
    ```

5. Encrypt the files  
    ```bash
    git-crypt status -f
    ```
   
6. Commit the project and check that the files are encrypted in git

7. Optional: Clone the project in a different directory and follow the first instructions to decrypt the files.

## References
- https://github.com/AGWA/git-crypt
- https://buddy.works/guides/git-crypt
